#### DAY01/EX05 (TESTER)

this is a simple testing script for [day01/ex05](https://github.com/barrak7/Python-4DS-Bootcamp/blob/main/subjects/1%20-%20Array/1%20-%20Arrary.pdf) ... it just display's
the original image and the result so you can check if the filter is applied or no !

##### Setup

```sh
pip install inquirer
```

##### Usage

```sh
~> python3 test.py
```

- just execute the script
- then select you're options
- note: the script locates the image in the current (.) working directory !
- note: and it will only display .jpg and .jpeg in the current (.) working directory !
- mohim check it out if u face any issues or can't run it properly, ping me or dm !
- here's a screenshot ^^

![screenshot_ehe~](/assets/sc.png)
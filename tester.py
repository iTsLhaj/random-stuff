import numpy as np
import cv2 as cv
import inquirer
import os

from pimp_image import *


def main(image_path: str, tfilter_: str) -> None:

    img = cv.imread(image_path)
    filers_ = {
        "invert": ft_invert,
        "red": ft_red,
        "green": ft_green,
        "blue": ft_blue,
        "grey": ft_grey
    }

    if tfilter_ not in filers_.keys():
        print("AssertionError: Wrong filter type!")
        exit(1)

    filter_ = filers_[tfilter_](img)

    cv.namedWindow("Original", cv.WINDOW_NORMAL)
    cv.namedWindow(tfilter_.capitalize() + " Filter", cv.WINDOW_NORMAL)

    cv.imshow("Original", img)
    cv.imshow(tfilter_.capitalize() + " Filter", filter_)

    if cv.waitKey(0):
        cv.destroyAllWindows()


if __name__ == "__main__":

    questions = [
        inquirer.List(
            name="image_path",
            message="choose an image to try !",
            choices=[i for i in os.listdir() if str(i).endswith('jpg') or str(i).endswith('jpeg')],
            default=None
        ),
        inquirer.List(
            name="target_test",
            message="choose a test to try !",
            choices=[
                "invert",
                "red",
                "green",
                "blue",
                "grey"
            ],
            default="invert"
        )
    ]
    answers = inquirer.prompt(questions)
    if (answers["image_path"] == None):
        print("AssertionError: No image path!")
        exit(1)
    else:
        if os.path.exists(answers["image_path"]) == False:
            print("AssertionError:", end=" ")
            print("The image doesn't even exist 💀 !!!")
            exit(1)
        else:
            main(
                image_path=answers["image_path"],
                tfilter_=answers["target_test"]
            )